# Application registry
Microservice for registering new applications.

## Known paths
### application instance
`GET application-registry/V1/application-instance/user`

Return
```json
[
  {
    "id": 1,
    "name": "",
    "application_type_id": 2,
    "application_type_name": "",
    "application_path": "",
    "application_type_thumbnail": "",
    "user_ids": [],
    "organizations": [],
    "activated": true
  }
]
```

`GET application-registry/V1/application-instance/internal/checkPermission?userId=&applicationId=`

Return
```json
[
  {
    "id": 1,
    "name": "",
    "application_type_id": 2,
    "application_type_name": "",
    "application_path": "",
    "application_type_thumbnail": "",
    "user_ids": [],
    "organizations": [],
    "activated": true
  }
]
```

`POST application-registry/V1/application-instance`

Body
```json
[
  {
    "name": "",
    "application_type_id": 2,
    "application_type_name": "",
    "application_path": "",
    "application_type_thumbnail": "",
    "user_ids": [],
    "organizations": [],
    "activated": true
  }
]
```

`PUT application-registry/V1/application-instance/{applicationInstanceId}`

Body
```json
[
  {
    "name": "",
    "application_type_id": 2,
    "application_type_name": "",
    "application_path": "",
    "application_type_thumbnail": "",
    "user_ids": [],
    "organizations": [],
    "activated": true
  }
]
```

`DELETE application-registry/V1/application-instance/{applicationInstanceId}`

### application type
`GET application-registry/V1/application-type`

Return
```json
[
  {
    "id": 1,
    "name": "",
    "description": "",
    "application_path": "",
    "thumbnail": ""
  }
]
```

## DB
Database migration implemented with [flyway](https://flywaydb.org/)
Scrips in `resources/db/migration`.

## Feign
Communicates with the `users-and-organizations` microservice to get the family (all the user's parents) of users for 
each given user id.

`GET users-and-organizations/V1/user/internal/device-users`

Return
```json
[
  {"id": 1}
]
```

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry.

## Tests
Groovy tests in `src/test/groovy`.