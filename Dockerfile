FROM anapsix/alpine-java:8
ADD target/application-registry.jar application-registry.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /application-registry.jar
