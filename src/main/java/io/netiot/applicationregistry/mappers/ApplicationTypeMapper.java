package io.netiot.applicationregistry.mappers;

import io.netiot.applicationregistry.entities.ApplicationType;
import io.netiot.applicationregistry.models.ApplicationTypeListModel;
import io.netiot.applicationregistry.models.ApplicationTypeModel;

import java.util.List;

public final class ApplicationTypeMapper {

    private ApplicationTypeMapper() {
    }

    public static ApplicationTypeModel toModel(final ApplicationType applicationType) {
        return ApplicationTypeModel.builder()
                .id(applicationType.getId())
                .name(applicationType.getName())
                .description(applicationType.getDescription())
                .applicationPath(applicationType.getApplicationPath())
                .thumbnail(applicationType.getThumbnail())
                .build();
    }

    public static ApplicationTypeListModel toListModel(final List<ApplicationTypeModel> applicationTypes) {
        return ApplicationTypeListModel.builder()
                .applicationTypes(applicationTypes)
                .build();
    }

}
