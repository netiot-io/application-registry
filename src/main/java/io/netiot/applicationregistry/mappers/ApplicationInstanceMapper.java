package io.netiot.applicationregistry.mappers;

import io.netiot.applicationregistry.entities.ApplicationInstance;
import io.netiot.applicationregistry.entities.ApplicationUsers;
import io.netiot.applicationregistry.models.ApplicationInstanceExternalModel;
import io.netiot.applicationregistry.models.ApplicationInstanceListModel;
import io.netiot.applicationregistry.models.ApplicationInstanceModel;
import io.netiot.applicationregistry.models.StatusType;

import java.util.List;
import java.util.stream.Collectors;

public final class ApplicationInstanceMapper {

    private ApplicationInstanceMapper() {
    }

    public static ApplicationInstance toEntity(final ApplicationInstanceModel applicationInstanceModel) {
        return ApplicationInstance.builder()
                .id(applicationInstanceModel.getId())
                .name(applicationInstanceModel.getName())
                .activated(applicationInstanceModel.getActivated())
                .users(applicationInstanceModel.getUserIds().stream().map(userId -> ApplicationUsers.builder().userId(userId).build()).collect(Collectors.toSet()))
                .build();
    }

    public static ApplicationInstanceModel toModel(final ApplicationInstance applicationInstance) {
        return ApplicationInstanceModel.builder()
                .id(applicationInstance.getId())
                .name(applicationInstance.getName())
                .applicationTypeName(applicationInstance.getApplicationType().getName())
                .applicationPath(applicationInstance.getApplicationType().getApplicationPath())
                .applicationTypeThumbnail(applicationInstance.getApplicationType().getThumbnail())
                .applicationTypeId(applicationInstance.getApplicationType().getId())
                .activated(applicationInstance.getActivated())
                .userIds(applicationInstance.getUsers().stream().map(ApplicationUsers::getUserId).collect(Collectors.toList()))
                .build();
    }

    public static ApplicationInstanceListModel toListModel(final List<ApplicationInstanceModel> applicationInstanceModels) {
        return ApplicationInstanceListModel.builder()
                .applicationInstances(applicationInstanceModels)
                .build();
    }

    public static ApplicationInstanceExternalModel toExternalModel(final ApplicationInstance applicationInstance){
        return ApplicationInstanceExternalModel.builder()
                .id(applicationInstance.getId())
                .name(applicationInstance.getName())
                .statusType(applicationInstance.getActivated() ? StatusType.ENABLE : StatusType.DISABLE)
                .build();
    }

}
