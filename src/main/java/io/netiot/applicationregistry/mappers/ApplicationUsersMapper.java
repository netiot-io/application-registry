package io.netiot.applicationregistry.mappers;

import io.netiot.applicationregistry.entities.ApplicationInstance;
import io.netiot.applicationregistry.entities.ApplicationUsers;

import java.util.Set;
import java.util.stream.Collectors;

public class ApplicationUsersMapper {

    private ApplicationUsersMapper() {
    }

    public static Set<ApplicationUsers> toEntityList(final Set<Long> usersIds, final ApplicationInstance applicationInstance) {
        return usersIds.stream().map(userId -> ApplicationUsers.builder().userId(userId).applicationInstance(applicationInstance).build()).collect(Collectors.toSet());
    }
}
