package io.netiot.applicationregistry.mappers;

import io.netiot.applicationregistry.entities.User;
import io.netiot.applicationregistry.models.UserInternalModel;

public final class UserMapper {

    private UserMapper() {
    }

    public static User toEntity(final UserInternalModel userInternalModel) {
        return User.builder()
                .id(userInternalModel.getId())
                .organizationId((userInternalModel.getOrganizationId()))
                .build();
    }

}