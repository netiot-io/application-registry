package io.netiot.applicationregistry.utils;

public enum JwtEnum {
    PLATFORM ("platform"),
    USER_ID("userId"),
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"),
    EMAIL("email"),
    ROLE("role"),
    ORGANIZATION_ID("organizationId"),
    ORGANIZATION_NAME("organizationName");

    private final String propertyName;

    JwtEnum(final String propertyName) {
        this.propertyName = propertyName;
    }

    @Override
    public String toString() {
        return this.propertyName;
    }

}