package io.netiot.applicationregistry.utils;

import io.netiot.applicationregistry.entities.Role;
import io.netiot.applicationregistry.exceptions.JwtException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Component
public class AuthenticatedUserInfo {

    private final TokenStore tokenStore;

    public AuthenticatedUserInfo(final TokenStore tokenStore) {
        this.tokenStore = Objects.requireNonNull(tokenStore,"tokenStore must not be null.");
    }

    private Map<String, Object> getExtraInfo() {
        OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) oAuth2Authentication.getDetails();
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(details.getTokenValue());
        return accessToken.getAdditionalInformation();
    }

    private String getUserInfo(final JwtEnum jwtEnum){
        return Optional.of(getExtraInfo().get(jwtEnum.toString())).map(Object::toString).orElseThrow(() -> {
           throw new JwtException("Jwt doesn't contain " + jwtEnum.toString() + " information.");
        });
    }

    public Role getRole(){
        return Role.valueOf(getUserInfo(JwtEnum.ROLE));
    }

    public String getFirstName(){
        return getUserInfo(JwtEnum.FIRST_NAME);
    }

    public String getLastName(){
        return getUserInfo(JwtEnum.LAST_NAME);
    }

    public String getEmail(){
        return getUserInfo(JwtEnum.EMAIL);
    }

    public String getPlatform(){
        return getUserInfo(JwtEnum.PLATFORM);
    }

    public Long getId(){
        return Long.valueOf(getUserInfo(JwtEnum.USER_ID));
    }

    public Long getOrganizationId(){
        return Long.valueOf(getUserInfo(JwtEnum.ORGANIZATION_ID));
    }

    public Long getOrganizationName(){
        return Long.valueOf(getUserInfo(JwtEnum.ORGANIZATION_NAME));
    }

}
