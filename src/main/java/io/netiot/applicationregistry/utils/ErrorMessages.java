package io.netiot.applicationregistry.utils;

public final class ErrorMessages {

    public static final String UNAUTHORIZED_ACTION_ERROR_CODE = "backend.app.registry.unauthorized.action";

    public static final String APPLICATION_TYPE_NOT_EXIST_ERROR_CODE = "backend.app.registry.application.type.not.exist";

    public static final String APPLICATION_INSTANCE_NOT_EXIST_ERROR_CODE = "backend.app.registry.application.instance.not.exist";

    public static final String INTERNAL_ERROR = "backend.app.registry.internal.error";

    public static final String USER_NOT_EXIST_ERROR_CODE = "backend.app.registry.user.not.exist";

}
