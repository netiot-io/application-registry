package io.netiot.applicationregistry.entities;

public enum Role {
    ADMIN, FREE_USER, PREMIUM_USER, ENTERPRISE_USER, ENTERPRISE_ADMIN, PARTNER_USER, PARTNER_ADMIN
}