package io.netiot.applicationregistry.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "APPLICATION_INSTANCES_USERS")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(exclude = "applicationInstance")
@ToString(exclude = "applicationInstance")
public class ApplicationUsers {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_APPLICATION_INSTANCES_USER_GEN")
    @SequenceGenerator(name = "SEQ_APPLICATION_INSTANCES_USER_GEN", sequenceName = "SEQ_APPLICATION_INSTANCES_USER", allocationSize = 1)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_instance_id")
    private ApplicationInstance applicationInstance;
}
