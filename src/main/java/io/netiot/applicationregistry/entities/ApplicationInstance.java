package io.netiot.applicationregistry.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "APPLICATION_INSTANCES")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApplicationInstance {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_APPLICATION_INSTANCE_GEN")
    @SequenceGenerator(name = "SEQ_APPLICATION_INSTANCE_GEN", sequenceName = "SEQ_APPLICATION_INSTANCE", allocationSize = 1)
    private Long id;

    private String name;

    @ManyToOne(cascade= CascadeType.DETACH)
    @JoinColumn(name = "application_type_id")
    private ApplicationType applicationType;

    @OneToMany(mappedBy = "applicationInstance", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<ApplicationUsers> users;

    private Boolean activated;

}
