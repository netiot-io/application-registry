package io.netiot.applicationregistry.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "APPLICATION_TYPES")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApplicationType {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_APPLICATION_TYPE_GEN")
    @SequenceGenerator(name = "SEQ_APPLICATION_TYPE_GEN", sequenceName = "SEQ_APPLICATION_TYPE", allocationSize = 1)
    private Long id;

    private String name;

    private String description;

    private String serviceName;

    private String applicationPath;

    private String thumbnail;

}
