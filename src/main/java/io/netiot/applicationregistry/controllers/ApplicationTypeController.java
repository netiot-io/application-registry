package io.netiot.applicationregistry.controllers;

import io.netiot.applicationregistry.mappers.ApplicationTypeMapper;
import io.netiot.applicationregistry.models.ApplicationTypeModel;
import io.netiot.applicationregistry.services.ApplicationTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/application-type")
@RequiredArgsConstructor
public class ApplicationTypeController {

    private final ApplicationTypeService applicationTypeService;

    @GetMapping
    public ResponseEntity getApplicationsTypes(){
        List<ApplicationTypeModel> applicationTypeModels = applicationTypeService.getApplicationsTypes();
        return ResponseEntity.ok(ApplicationTypeMapper.toListModel(applicationTypeModels));
    }

}