package io.netiot.applicationregistry.controllers;

import io.netiot.applicationregistry.mappers.ApplicationInstanceMapper;
import io.netiot.applicationregistry.models.ApplicationInstanceModel;
import io.netiot.applicationregistry.services.ApplicationInstanceService;
import io.netiot.applicationregistry.validators.ValidatorUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/application-instance")
@RequiredArgsConstructor
public class ApplicationInstanceController {

    private final ValidatorUtil validatorUtil;
    private final ApplicationInstanceService applicationInstanceService;

    @PreAuthorize("hasAnyAuthority('ENTERPRISE_ADMIN', 'ENTERPRISE_USER','FREE_USER', 'PREMIUM_USER')")
    @GetMapping("/user")
    public ResponseEntity getApplicationInstancesForCurrentUser() {
        List<ApplicationInstanceModel> applicationInstanceModels = applicationInstanceService.getApplicationInstancesCurrentUser();
        return ResponseEntity.ok(ApplicationInstanceMapper.toListModel(applicationInstanceModels));
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('PARTNER_ADMIN', 'PARTNER_USER', 'PREMIUM_USER', 'FREE_USER')")
    public ResponseEntity saveApplicationInstance(@RequestBody @Valid final ApplicationInstanceModel createApplicationInstanceModel,
                                                  final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        ApplicationInstanceModel applicationInstanceModel = applicationInstanceService.save(createApplicationInstanceModel);
        return ResponseEntity.ok(generateMap(applicationInstanceModel));
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER')")
    @PutMapping("/{applicationInstanceId}")
    public ResponseEntity updateApplicationInstance(@PathVariable(name = "applicationInstanceId") Long applicationInstanceId,
                                                    @RequestBody @Valid final ApplicationInstanceModel updateApplicationInstanceModel,
                                                    final BindingResult bindingResult) {
        validatorUtil.checkBindingResultErrors(bindingResult);
        applicationInstanceService.update(applicationInstanceId, updateApplicationInstanceModel);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN','PARTNER_ADMIN','PARTNER_USER')")
    @DeleteMapping("/{applicationInstanceId}")
    public ResponseEntity deleteApplicationInstance(@PathVariable(name = "applicationInstanceId") Long applicationInstanceId) {
        applicationInstanceService.delete(applicationInstanceId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/internal/checkPermission")
    public ResponseEntity checkPermission(@RequestParam(name = "userId") final Long userId,
                                      @RequestParam(name = "applicationId") final Long applicationId) {
        boolean b = applicationInstanceService.checkPermission(userId, applicationId);
        return ResponseEntity.ok(b);
    }

    private Map<String, Object> generateMap(final ApplicationInstanceModel applicationInstanceModel) {
        Map<String, Object> result = new HashMap<>();
        result.put("id", applicationInstanceModel.getId());
        return result;
    }

}
