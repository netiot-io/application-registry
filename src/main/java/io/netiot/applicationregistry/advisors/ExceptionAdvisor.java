package io.netiot.applicationregistry.advisors;

import io.netiot.applicationregistry.exceptions.*;
import io.netiot.applicationregistry.models.ErrorModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice
public class ExceptionAdvisor {

    @ExceptionHandler(value = {ApplicationInstanceException.class})
    public ResponseEntity errorHandle(final ApplicationInstanceException applicationInstanceException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", applicationInstanceException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {UserException.class})
    public ResponseEntity errorHandle(final UserException userException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", userException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {JwtException.class})
    public ResponseEntity errorHandle(final JwtException jwtException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", jwtException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {BindingResultException.class})
    public ResponseEntity errorHandle(final BindingResultException bindingResultRuntimeException) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorModel
                        .builder()
                        .error(bindingResultRuntimeException.getErrors())
                        .build());
    }


    @ExceptionHandler(value = {ApplicationTypeException.class})
    public ResponseEntity errorHandle(final ApplicationTypeException applicationTypeException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", applicationTypeException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {UnauthorizedOperationException.class})
    public ResponseEntity errorHandle(final UnauthorizedOperationException unauthorizedOperationException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", unauthorizedOperationException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

    @ExceptionHandler(value = {InternalErrorException.class})
    public ResponseEntity errorHandle(final InternalErrorException internalErrorException) {
        HashMap<String, String> body = new HashMap<>();
        body.put("error", internalErrorException.getMessage());
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

}
