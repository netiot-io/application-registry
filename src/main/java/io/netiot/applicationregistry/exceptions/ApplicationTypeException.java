package io.netiot.applicationregistry.exceptions;

public class ApplicationTypeException extends RuntimeException {

    public ApplicationTypeException() {
    }

    public ApplicationTypeException(String message) {
        super(message);
    }

}
