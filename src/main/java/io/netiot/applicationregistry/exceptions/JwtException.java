package io.netiot.applicationregistry.exceptions;

public class JwtException extends RuntimeException {

    public JwtException() {
    }

    public JwtException(String message) {
        super(message);
    }
}
