package io.netiot.applicationregistry.exceptions;

public class InternalErrorException extends RuntimeException {

    public InternalErrorException() {
    }

    public InternalErrorException(String message) {
        super(message);
    }

}
