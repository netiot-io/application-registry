package io.netiot.applicationregistry.exceptions;

public class ApplicationInstanceException extends RuntimeException {

    public ApplicationInstanceException() {
    }

    public ApplicationInstanceException(String message) {
        super(message);
    }
}
