package io.netiot.applicationregistry.exceptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BindingResultException extends RuntimeException {

   private final List<String> errors;

    public BindingResultException(final String error) {
        this.errors = new ArrayList<>();
        this.errors.add(error);
    }

    public BindingResultException(final List<String> errors) {
        this.errors = Objects.requireNonNull(errors, "error must be not null.");
    }

    public List<String> getErrors() {
        return errors;
    }
}
