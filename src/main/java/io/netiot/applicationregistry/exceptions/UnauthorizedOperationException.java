package io.netiot.applicationregistry.exceptions;

public class UnauthorizedOperationException extends RuntimeException {

    public UnauthorizedOperationException() {
    }

    public UnauthorizedOperationException(String message) {
        super(message);
    }
}
