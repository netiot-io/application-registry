package io.netiot.applicationregistry.feign;

import io.netiot.applicationregistry.models.UserInternalModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "users-and-organizations")
public interface UsersClient {

    @RequestMapping(method = RequestMethod.GET, value = "V1/user/internal/device-users")
    ResponseEntity<List<UserInternalModel>> getUsers(@RequestParam final List<Long> userIds);

}
