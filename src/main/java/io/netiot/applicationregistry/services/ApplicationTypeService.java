package io.netiot.applicationregistry.services;

import io.netiot.applicationregistry.entities.ApplicationType;
import io.netiot.applicationregistry.exceptions.ApplicationTypeException;
import io.netiot.applicationregistry.mappers.ApplicationTypeMapper;
import io.netiot.applicationregistry.models.ApplicationTypeModel;
import io.netiot.applicationregistry.repositories.ApplicationTypeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.netiot.applicationregistry.utils.ErrorMessages.APPLICATION_TYPE_NOT_EXIST_ERROR_CODE;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ApplicationTypeService {

    private final ApplicationTypeRepository applicationTypeRepository;

    public Optional<ApplicationType> findByApplicationTypeId(final Long applicationTypeId) {
        return applicationTypeRepository.findById(applicationTypeId);
    }

    public List<ApplicationTypeModel> getApplicationsTypes() {
        return applicationTypeRepository.findAll()
                .stream()
                .map(ApplicationTypeMapper::toModel)
                .collect(Collectors.toList());
    }
    
}
