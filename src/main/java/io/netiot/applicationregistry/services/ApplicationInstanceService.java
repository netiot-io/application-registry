package io.netiot.applicationregistry.services;

import io.netiot.applicationregistry.entities.ApplicationInstance;
import io.netiot.applicationregistry.entities.ApplicationType;
import io.netiot.applicationregistry.entities.Role;
import io.netiot.applicationregistry.entities.User;
import io.netiot.applicationregistry.exceptions.ApplicationInstanceException;
import io.netiot.applicationregistry.exceptions.ApplicationTypeException;
import io.netiot.applicationregistry.mappers.ApplicationInstanceMapper;
import io.netiot.applicationregistry.mappers.ApplicationUsersMapper;
import io.netiot.applicationregistry.models.ApplicationInstanceModel;
import io.netiot.applicationregistry.repositories.ApplicationInstanceRepository;
import io.netiot.applicationregistry.repositories.ApplicationUsersRepository;
import io.netiot.applicationregistry.utils.AuthenticatedUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.netiot.applicationregistry.utils.ErrorMessages.*;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ApplicationInstanceService {

    private final ApplicationInstanceRepository applicationInstanceRepository;
    private final ApplicationUsersRepository applicationUsersRepository;
    private final AuthenticatedUserInfo authenticatedUserInfo;
    private final ApplicationTypeService applicationTypeService;
    private final UserService userService;

    public ApplicationInstanceModel save(final ApplicationInstanceModel applicationInstanceModel) {
        ApplicationType applicationType = applicationTypeService.findByApplicationTypeId(applicationInstanceModel.getApplicationTypeId())
         .orElseThrow(() -> { throw new ApplicationTypeException(APPLICATION_TYPE_NOT_EXIST_ERROR_CODE); });

        List<User> parentUsers = userService.findUsersByIds(applicationInstanceModel.getUserIds())
                .orElseThrow(() -> { throw new ApplicationInstanceException(USER_NOT_EXIST_ERROR_CODE); });

        Role role = authenticatedUserInfo.getRole();
        if(role == Role.PREMIUM_USER || role == Role.FREE_USER){
            throw new ApplicationInstanceException(role + " create application instance error.");
        }

        ApplicationInstance applicationInstance = ApplicationInstanceMapper.toEntity(applicationInstanceModel);

        applicationUsersRepository.deleteByApplicationInstance_Id(applicationInstance.getId());
        applicationInstance.setUsers(ApplicationUsersMapper.toEntityList(parentUsers.stream().map(User::getId).collect(Collectors.toSet()), applicationInstance));
        applicationInstance.setApplicationType(applicationType);

        if(role != Role.ADMIN){
            Long userId = authenticatedUserInfo.getId();
            if (applicationInstance.getUsers().stream().noneMatch(user -> user.getUserId().compareTo(userId) == 0)) {
                throw new ApplicationInstanceException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }

        applicationInstance = applicationInstanceRepository.save(applicationInstance);

        return ApplicationInstanceMapper.toModel(applicationInstance);
    }

    public void update(final Long applicationInstanceId, final ApplicationInstanceModel applicationInstanceModel) {
        ApplicationInstance applicationInstance = validateRequest(applicationInstanceId);

        ApplicationType applicationType = applicationTypeService.findByApplicationTypeId(applicationInstanceModel.getApplicationTypeId())
                .orElseThrow(() -> { throw new ApplicationTypeException(APPLICATION_TYPE_NOT_EXIST_ERROR_CODE); });

        List<User> parentUsers = userService.findUsersByIds(applicationInstanceModel.getUserIds())
                .orElseThrow(() -> { throw new ApplicationInstanceException(USER_NOT_EXIST_ERROR_CODE); });

        Role role = authenticatedUserInfo.getRole();
        if(role == Role.PREMIUM_USER || role == Role.FREE_USER){
            throw new ApplicationInstanceException(role + " create application instance error.");
        }

        applicationInstance.setName(applicationInstanceModel.getName());
        applicationUsersRepository.deleteByApplicationInstance_Id(applicationInstance.getId());
        applicationInstance.setUsers(ApplicationUsersMapper.toEntityList(parentUsers.stream().map(User::getId).collect(Collectors.toSet()), applicationInstance));
        applicationInstance.setApplicationType(applicationType);
        applicationInstance.setActivated(applicationInstanceModel.getActivated());

        applicationInstanceRepository.save(applicationInstance);
    }

    private ApplicationInstance validateRequest(final Long applicationInstanceId) {
        ApplicationInstance applicationInstance = applicationInstanceRepository.findById(applicationInstanceId)
                .orElseThrow(() -> { throw new ApplicationInstanceException(APPLICATION_INSTANCE_NOT_EXIST_ERROR_CODE); });

        Role userRole = authenticatedUserInfo.getRole();

        if(userRole != Role.ADMIN){
            Long userId = authenticatedUserInfo.getId();
            if (applicationInstance.getUsers().stream().noneMatch(user -> user.getUserId().compareTo(userId) == 0)) {
                throw new ApplicationInstanceException(UNAUTHORIZED_ACTION_ERROR_CODE);
            }
        }

        return applicationInstance;
    }

    public List<ApplicationInstanceModel> getApplicationInstancesCurrentUser() {
        Long userId = authenticatedUserInfo.getId();
        return applicationInstanceRepository.findByUsers_UserId(userId).stream()
                .map(application -> {
                    ApplicationInstanceModel appModel = ApplicationInstanceMapper.toModel(application);
                    Optional<List<User>> users = userService.findUsersByIds(appModel.getUserIds());
                    users.ifPresent(userList -> appModel.setOrganizations(userList.stream().map(User::getOrganizationId).collect(Collectors.toSet())));
                    return appModel;
                })
                .collect(Collectors.toList());
    }

    public void delete(final Long applicationInstanceId) {
        ApplicationInstance applicationInstance = validateRequest(applicationInstanceId);

        applicationInstanceRepository.deleteById(applicationInstanceId);
    }

    public boolean checkPermission(final Long userId, final Long applicationId) {
        ApplicationInstance applicationInstance = applicationInstanceRepository.findById(applicationId)
                .orElseThrow(() -> { throw new ApplicationInstanceException(APPLICATION_INSTANCE_NOT_EXIST_ERROR_CODE); });

        return applicationInstance.getUsers().stream().anyMatch(user -> user.getUserId().compareTo(userId) == 0);
    }

}
