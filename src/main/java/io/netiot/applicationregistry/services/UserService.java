package io.netiot.applicationregistry.services;

import feign.FeignException;
import io.netiot.applicationregistry.entities.User;
import io.netiot.applicationregistry.exceptions.InternalErrorException;
import io.netiot.applicationregistry.feign.UsersClient;
import io.netiot.applicationregistry.mappers.UserMapper;
import io.netiot.applicationregistry.models.UserInternalModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.netiot.applicationregistry.utils.ErrorMessages.INTERNAL_ERROR;


@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class UserService {

    private final Integer HTTP_STATUS_NOT_FOUND = 404;

    private final UsersClient usersClient;

    public Optional<List<User>> findUsersByIds(final List<Long> userIds) {
        try {
            ResponseEntity<List<UserInternalModel>> userModelResponseEntity = usersClient.getUsers(userIds);
            List<UserInternalModel> usersInternalModel = userModelResponseEntity.getBody();
            List<User> users = usersInternalModel.stream().map(UserMapper::toEntity).collect(Collectors.toList());

            return Optional.of(users);
        } catch (FeignException e) {

            if (e.status() == HTTP_STATUS_NOT_FOUND) {
                return Optional.empty();
            }

            log.error("Internal Error", e);

            throw new InternalErrorException(INTERNAL_ERROR);
        } catch (Exception e) {
            log.error("Internal Error", e);

            throw new InternalErrorException(INTERNAL_ERROR);
        }
    }

}
