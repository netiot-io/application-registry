package io.netiot.applicationregistry.repositories;

import io.netiot.applicationregistry.entities.ApplicationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationTypeRepository extends JpaRepository<ApplicationType,Long> {


}
