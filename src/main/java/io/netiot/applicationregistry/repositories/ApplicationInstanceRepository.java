package io.netiot.applicationregistry.repositories;

import io.netiot.applicationregistry.entities.ApplicationInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ApplicationInstanceRepository extends JpaRepository<ApplicationInstance,Long> {

    Optional<ApplicationInstance> findByName(final String name);

    List<ApplicationInstance> findByUsers_UserId(final Long userId);
}
