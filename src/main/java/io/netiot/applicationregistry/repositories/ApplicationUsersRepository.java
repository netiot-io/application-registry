package io.netiot.applicationregistry.repositories;

import io.netiot.applicationregistry.entities.ApplicationUsers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ApplicationUsersRepository extends JpaRepository<ApplicationUsers, Long> {

        @Modifying
        @Query("delete from ApplicationUsers au where au.applicationInstance.id = ?1")
        void deleteByApplicationInstance_Id(final Long applicationId);
}
