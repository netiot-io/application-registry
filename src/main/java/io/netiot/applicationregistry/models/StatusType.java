package io.netiot.applicationregistry.models;

public enum StatusType {
    ENABLE, DISABLE
}