package io.netiot.applicationregistry.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.netiot.applicationregistry.entities.ApplicationUsers;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ApplicationInstanceExternalModel {

    private Long id;

    private String name;

    private Set<ApplicationUsers> users;

    private StatusType statusType;

}
