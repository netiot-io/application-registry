package io.netiot.applicationregistry.models;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ApplicationInstanceModel {

    private Long id;

    @NotEmpty(message = "backend.app.registry.application.instance.name.notnull.not.empty")
    @Size(min = 3, message = "backend.app.registry.application.instance.name.size")
    private String name;

    @NotNull(message = "backend.app.registry.application.application.type.id.notnull")
    private Long applicationTypeId;

    private String applicationTypeName;

    private String applicationPath;

    private String applicationTypeThumbnail;

    private List<Long> userIds;

    private Set<Long> organizations;

    @NotNull(message = "backend.app.registry.application.activated.notnull")
    private Boolean activated;

}
