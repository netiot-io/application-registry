ALTER TABLE APPLICATION_INSTANCES ADD COLUMN organization_created_by BIGINT;
ALTER TABLE APPLICATION_INSTANCES ADD COLUMN organization_type CHARACTER VARYING(20);

UPDATE APPLICATION_INSTANCES SET organization_created_by = q.created_by, organization_type = q.organization_type, organization_id =q.organization_id
FROM (SELECT o.id, o.organization_id, o.created_by, o.organization_type FROM ORGANIZATIONS o, APPLICATION_INSTANCES a
      WHERE o.id = a.organization_id) q
WHERE APPLICATION_INSTANCES.organization_id = q.id;

ALTER TABLE APPLICATION_INSTANCES DROP CONSTRAINT application_instances_organization_id_fkey;

DROP TABLE ORGANIZATIONS;
DROP SEQUENCE SEQ_ORGANIZATION;