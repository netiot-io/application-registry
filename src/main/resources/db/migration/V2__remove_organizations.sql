ALTER TABLE APPLICATION_INSTANCES
DROP COLUMN organization_id,
DROP COLUMN organization_created_by,
DROP COLUMN organization_type;