CREATE SEQUENCE SEQ_APPLICATION_TYPE;

CREATE TABLE APPLICATION_TYPES (
  id          BIGINT                 PRIMARY KEY,
  name        CHARACTER VARYING(100) UNIQUE NOT NULL,
  description VARCHAR(500)           NOT NULL,
  topic       VARCHAR(100)           NOT NULL
);

CREATE SEQUENCE SEQ_ORGANIZATION;

CREATE TABLE ORGANIZATIONS (
  id                BIGINT PRIMARY KEY,
  organization_id   BIGINT UNIQUE NOT NULL,
  organization_type CHARACTER VARYING(20) NOT NULL,
  created_by         BIGINT,
  deleted            BOOLEAN DEFAULT FALSE
);

CREATE SEQUENCE SEQ_APPLICATION_INSTANCE;

CREATE TABLE APPLICATION_INSTANCES (
  id                  BIGINT                PRIMARY KEY,
  name                CHARACTER VARYING(50) NOT NULL,
  application_type_id BIGINT                NOT NULL,
  organization_id     BIGINT                NOT NULL,
  activated           BOOLEAN               NOT NULL DEFAULT FALSE,
  FOREIGN KEY (application_type_id) REFERENCES APPLICATION_TYPES (id),
  FOREIGN KEY (organization_id)     REFERENCES ORGANIZATIONS (id)
);

CREATE SEQUENCE SEQ_APPLICATION_INSTANCES_USER;

CREATE TABLE APPLICATION_INSTANCES_USERS (
  id                      BIGINT,
  application_instance_id BIGINT NOT NULL,
  user_id                 BIGINT NOT NULL,
  PRIMARY KEY (application_instance_id, user_id)
);