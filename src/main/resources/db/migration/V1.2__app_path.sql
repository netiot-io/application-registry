alter table application_types add application_path VARCHAR(100) NOT NULL DEFAULT '';
ALTER TABLE application_types ALTER COLUMN application_path DROP DEFAULT;