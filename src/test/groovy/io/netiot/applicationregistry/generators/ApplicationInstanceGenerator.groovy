package io.netiot.applicationregistry.generators

import io.netiot.applicationregistry.entities.ApplicationInstance
import io.netiot.applicationregistry.entities.ApplicationUsers
import io.netiot.applicationregistry.models.ApplicationInstanceExternalModel
import io.netiot.applicationregistry.models.ApplicationInstanceListModel
import io.netiot.applicationregistry.models.ApplicationInstanceModel
import io.netiot.applicationregistry.models.StatusType

import static io.netiot.applicationregistry.generators.ApplicationTypeGenerator.aApplicationType

class ApplicationInstanceGenerator {

    static aApplicationInstance(Map overrides = [:]) {
        Map values = [
                id: 1L,
                name : "applicationInstanceName",
                applicationType: aApplicationType(),
                activated: Boolean.FALSE,
                users: [new ApplicationUsers(null, 1L, null)]
        ]
        values << overrides
        return ApplicationInstance.newInstance(values)
    }

    static aApplicationInstanceModel(Map overrides = [:]) {
        def applicationType = aApplicationType()
        Map values = [
                id: 1L,
                name : "applicationInstanceName",
                applicationTypeName: applicationType.getName(),
                applicationPath: applicationType.getApplicationPath(),
                applicationTypeId: applicationType.getId(),
                activated: Boolean.FALSE,
                organizations: [1L],
                userIds: [1L]
        ]
        values << overrides
        return ApplicationInstanceModel.newInstance(values)
    }

    static aApplicationInstanceListModel(Map overrides = [:]){
        Map values = [
                applicationInstances: [ aApplicationInstanceModel() ]
        ]
        values << overrides
        return ApplicationInstanceListModel.newInstance(values)
    }

    static aApplicationInstanceExternalModel(Map overrides = [:]) {
        Map values = [
                id: 1L,
                name : "applicationInstanceName",
                statusType: StatusType.DISABLE
        ]
        values << overrides
        return ApplicationInstanceExternalModel.newInstance(values)
    }

}
