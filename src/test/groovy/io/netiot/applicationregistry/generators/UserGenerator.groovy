package io.netiot.applicationregistry.generators

import io.netiot.applicationregistry.entities.User
import io.netiot.applicationregistry.models.UserInternalModel

class UserGenerator {
    static aUser(Map overrides = [:]) {
        Map values = [
                id: 1L,
                organizationId: 1L
        ]
        values << overrides
        return User.newInstance(values)
    }

    static aUserInternalModel(Map overrides = [:]) {
        Map values = [
                id: 1L,
                organizationId: 1L
        ]
        values << overrides
        return UserInternalModel.newInstance(values)
    }
}
