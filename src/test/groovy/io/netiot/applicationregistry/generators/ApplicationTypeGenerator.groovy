package io.netiot.applicationregistry.generators

import io.netiot.applicationregistry.entities.ApplicationType
import io.netiot.applicationregistry.models.ApplicationTypeListModel
import io.netiot.applicationregistry.models.ApplicationTypeModel

class ApplicationTypeGenerator {

    static aApplicationType(Map overrides = [:]) {
        Map values = [
                id: 1L,
                name : "applicationTypeName",
                description: "description",
                applicationPath: "/V1/app-hotel",
                serviceName: "serviceName"
        ]
        values << overrides
        return ApplicationType.newInstance(values)
    }

    static aApplicationTypeModel(Map overrides = [:]) {
        Map values = [
                id: 1L,
                name : "applicationTypeName",
                applicationPath: "/V1/app-hotel",
                description: "description"
        ]
        values << overrides
        return ApplicationTypeModel.newInstance(values)
    }

    static aApplicationTypeListModel(Map overrides = [:]){
        Map values = [
                applicationTypes: [ aApplicationTypeModel() ]
        ]
        values << overrides
        return ApplicationTypeListModel.newInstance(values)
    }

}
