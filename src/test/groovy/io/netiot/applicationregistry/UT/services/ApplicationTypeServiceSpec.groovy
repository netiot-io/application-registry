package io.netiot.applicationregistry.UT.services

import io.netiot.applicationregistry.repositories.ApplicationTypeRepository
import io.netiot.applicationregistry.services.ApplicationTypeService

import spock.lang.Specification

import static io.netiot.applicationregistry.generators.ApplicationTypeGenerator.aApplicationType
import static io.netiot.applicationregistry.generators.ApplicationTypeGenerator.aApplicationTypeModel

class ApplicationTypeServiceSpec extends Specification{

    def applicationTypeRepository = Mock(ApplicationTypeRepository)

    def applicationTypeService = new ApplicationTypeService(applicationTypeRepository)

    def 'getApplicationsTypes'(){
        given:
        def applicationTypes = [ aApplicationType() ]
        def expectedResult = [ aApplicationTypeModel() ]

        when:
        def result = applicationTypeService.getApplicationsTypes()

        then:
        1 * applicationTypeRepository.findAll() >> applicationTypes
        0 * _

        result == expectedResult
    }

    def 'findByApplicationTypeId'() {
        given:
        def applicationType = aApplicationType()
        def applicationTypeId = applicationType.getId()
        def expectedResult = Optional.of(applicationType)

        when:
        def result = applicationTypeService.findByApplicationTypeId(applicationTypeId)

        then:
        1 * applicationTypeRepository.findById(applicationTypeId) >> Optional.of(applicationType)
        0 * _

        result == expectedResult
    }

}
