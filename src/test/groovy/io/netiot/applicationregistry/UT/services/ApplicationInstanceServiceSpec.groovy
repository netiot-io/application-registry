package io.netiot.applicationregistry.UT.services

import io.netiot.applicationregistry.entities.Role
import io.netiot.applicationregistry.exceptions.ApplicationInstanceException
import io.netiot.applicationregistry.exceptions.ApplicationTypeException
import io.netiot.applicationregistry.repositories.ApplicationInstanceRepository
import io.netiot.applicationregistry.repositories.ApplicationUsersRepository
import io.netiot.applicationregistry.services.ApplicationInstanceService
import io.netiot.applicationregistry.services.ApplicationTypeService

import io.netiot.applicationregistry.services.UserService
import io.netiot.applicationregistry.utils.AuthenticatedUserInfo
import spock.lang.Specification
import spock.lang.Unroll

import static io.netiot.applicationregistry.generators.ApplicationInstanceGenerator.aApplicationInstance
import static io.netiot.applicationregistry.generators.ApplicationInstanceGenerator.aApplicationInstanceModel
import static io.netiot.applicationregistry.generators.ApplicationTypeGenerator.aApplicationType
import static io.netiot.applicationregistry.generators.UserGenerator.aUser

class ApplicationInstanceServiceSpec extends Specification {

    def applicationInstanceRepository = Mock(ApplicationInstanceRepository)
    def userService = Mock(UserService)
    def authenticatedUserInfo = Mock(AuthenticatedUserInfo)
    def applicationTypeService = Mock(ApplicationTypeService)
    def applicationUsersRepository = Mock(ApplicationUsersRepository)

    def applicationInstanceService = new ApplicationInstanceService(applicationInstanceRepository,
            applicationUsersRepository, authenticatedUserInfo, applicationTypeService, userService)

    @Unroll
    def 'save'() {
        given:
        def userId = 1L
        def applicationInstanceModelReceived = aApplicationInstanceModel()

        def applicationTypeId = applicationInstanceModelReceived.getApplicationTypeId()
        def applicationType = aApplicationType(id: applicationTypeId)
        def user = aUser()
        def applicationInstance = aApplicationInstance(applicationType: applicationType)
        def applicationInstanceModelReturned = aApplicationInstanceModel(organizations: null)

        when:
        def result = applicationInstanceService.save(applicationInstanceModelReceived)

        then:
        1 * applicationTypeService.findByApplicationTypeId(applicationTypeId) >> Optional.of(applicationType)
        1 * userService.findUsersByIds([userId]) >> Optional.of([user])
        1 * authenticatedUserInfo.getRole() >> role
        1 * applicationUsersRepository.deleteByApplicationInstance_Id(applicationTypeId)
        getUserId * authenticatedUserInfo.getId() >> userId
        1 * applicationInstanceRepository.save(applicationInstance) >> applicationInstance
        0 * _
        result == applicationInstanceModelReturned

        where:
        role               | getUserId
        Role.ADMIN         | 0
        Role.PARTNER_USER  | 1
        Role.PARTNER_ADMIN | 1
    }

    def 'save - throw ApplicationTypeException - applicationType not found'() {
        given:
        def applicationInstanceModel = aApplicationInstanceModel()
        def applicationTypeId = applicationInstanceModel.getApplicationTypeId()

        when:
        applicationInstanceService.save(applicationInstanceModel)

        then:
        1 * applicationTypeService.findByApplicationTypeId(applicationTypeId) >> Optional.empty()
        0 * _
        thrown(ApplicationTypeException)
    }

    def 'save - throw ApplicationInstanceException - user not found'() {
        given:
        def usersIds = [1L]
        def applicationInstanceModel = aApplicationInstanceModel()
        def applicationTypeId = applicationInstanceModel.getApplicationTypeId()
        def applicationType = aApplicationType(id: applicationTypeId)

        when:
        applicationInstanceService.save(applicationInstanceModel)

        then:
        1 * applicationTypeService.findByApplicationTypeId(applicationTypeId) >> Optional.of(applicationType)
        1 * userService.findUsersByIds(usersIds) >> Optional.empty()
        0 * _
        thrown(ApplicationInstanceException)
    }

    @Unroll
    def 'save - throw ApplicationInstanceException - unimplemented function '() {
        given:
        def usersIds = [1L]
        def user = aUser()
        def applicationInstanceModel = aApplicationInstanceModel(organizations: null)
        def applicationTypeId = applicationInstanceModel.getApplicationTypeId()
        def applicationType = aApplicationType(id: applicationTypeId)

        when:
        applicationInstanceService.save(applicationInstanceModel)

        then:
        1 * applicationTypeService.findByApplicationTypeId(applicationTypeId) >> Optional.of(applicationType)
        1 * userService.findUsersByIds(usersIds) >> Optional.of([user])
        1 * authenticatedUserInfo.getRole() >> role
        0 * _
        thrown(ApplicationInstanceException)

        where:
        role << [Role.PREMIUM_USER, Role.FREE_USER]
    }

    def 'update where user is admin'() {
        given:
        def userIds = [1L]
        def applicationTypeId = 1L
        def newApplicationInstanceName = "newName"
        def applicationInstanceModel = aApplicationInstanceModel(name: newApplicationInstanceName)
        def applicationInstanceFromDataBase = aApplicationInstance()
        def applicationType = aApplicationType(id: applicationTypeId)
        def user = aUser()
        def savedApplicationInstance = aApplicationInstance(name: newApplicationInstanceName, applicationType: applicationType)
        def applicationInstanceId = applicationInstanceModel.getId()

        when:
        applicationInstanceService.update(applicationInstanceId, applicationInstanceModel)

        then:
        1 * applicationInstanceRepository.findById(applicationInstanceId) >> Optional.of(applicationInstanceFromDataBase)
        2 * authenticatedUserInfo.getRole() >> Role.ADMIN
        1 * applicationTypeService.findByApplicationTypeId(applicationTypeId) >> Optional.of(applicationType)
        1 * userService.findUsersByIds(userIds) >> Optional.of([user])
        1 * applicationUsersRepository.deleteByApplicationInstance_Id(applicationTypeId)
        1 * applicationInstanceRepository.save(savedApplicationInstance)
        0 * _
    }

    @Unroll
    def 'update where user is not admin'() {
        given:
        def userId = 1L
        def user = aUser()
        def applicationTypeId = 1L
        def newApplicationInstanceName = "newName"
        def applicationInstanceModel = aApplicationInstanceModel(name: newApplicationInstanceName, organizations: null)
        def applicationInstanceFromDataBase = aApplicationInstance()
        def applicationType = aApplicationType(id: applicationTypeId)
        def savedApplicationInstance = aApplicationInstance(name: newApplicationInstanceName, applicationType: applicationType)
        def applicationInstanceId = applicationInstanceModel.getId()

        when:
        applicationInstanceService.update(applicationInstanceId, applicationInstanceModel)

        then:
        1 * applicationInstanceRepository.findById(applicationInstanceId) >> Optional.of(applicationInstanceFromDataBase)
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getId() >> userId
        1 * applicationTypeService.findByApplicationTypeId(applicationTypeId) >> Optional.of(applicationType)
        1 * userService.findUsersByIds([userId]) >> Optional.of([user])
        1 * authenticatedUserInfo.getRole() >> role
        1 * applicationUsersRepository.deleteByApplicationInstance_Id(applicationTypeId)
        1 * applicationInstanceRepository.save(savedApplicationInstance)
        0 * _

        where:
        role << [ Role.PARTNER_ADMIN, Role.PARTNER_USER]
    }

    @Unroll
    def 'update where user is not admin - thrown ApplicationInstanceException'() {
        given:
        def userId = 2L
        def applicationInstanceModel = aApplicationInstanceModel()
        def applicationInstanceFromDataBase = aApplicationInstance()
        def applicationInstanceId = applicationInstanceModel.getId()

        when:
        applicationInstanceService.update(applicationInstanceId, applicationInstanceModel)

        then:
        1 * applicationInstanceRepository.findById(applicationInstanceId) >> Optional.of(applicationInstanceFromDataBase)
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getId() >> userId
        0 * _
        thrown(ApplicationInstanceException)

        where:
        role << [ Role.PARTNER_ADMIN, Role.PARTNER_USER]
    }

    def 'getApplicationInstancesCurrentAUser'() {
        given:
        def userId = 1L
        def user = aUser()
        def applicationInstances = [ aApplicationInstance() ]
        def expectedResult = [ aApplicationInstanceModel() ]

        when:
        def result = applicationInstanceService.getApplicationInstancesCurrentUser()

        then:
        1 * authenticatedUserInfo.getId() >> userId
        1 * applicationInstanceRepository.findByUsers_UserId(userId) >> applicationInstances
        1 * userService.findUsersByIds([userId]) >> Optional.of([user])
        0 * _

        result == expectedResult
    }

    def 'delete where user is admin'() {
        given:
        def applicationInstance = aApplicationInstance()
        def applicationInstanceId = applicationInstance.getId()


        when:
        applicationInstanceService.delete(applicationInstanceId)

        then:
        1 * applicationInstanceRepository.findById(applicationInstanceId) >> Optional.of(applicationInstance)
        1 * authenticatedUserInfo.getRole() >> Role.ADMIN
        1 * applicationInstanceRepository.deleteById(applicationInstanceId)
        0 * _
    }

    @Unroll
    def 'delete where user is not admin'() {
        given:
        def applicationInstance = aApplicationInstance()
        def applicationInstanceId = applicationInstance.getId()
        def userId = 1L

        when:
        applicationInstanceService.delete(applicationInstanceId)

        then:
        1 * applicationInstanceRepository.findById(applicationInstanceId) >> Optional.of(applicationInstance)
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getId() >> userId
        1 * applicationInstanceRepository.deleteById(applicationInstanceId)
        0 * _

        where:
        role << [ Role.PARTNER_ADMIN, Role.PARTNER_USER]
    }

    @Unroll
    def 'delete where user is not admin - thrown ApplicationInstanceException'() {
        given:
        def applicationInstance = aApplicationInstance()
        def applicationInstanceId = applicationInstance.getId()
        def userId = 10L

        when:
        applicationInstanceService.delete(applicationInstanceId)

        then:
        1 * applicationInstanceRepository.findById(applicationInstanceId) >> Optional.of(applicationInstance)
        1 * authenticatedUserInfo.getRole() >> role
        1 * authenticatedUserInfo.getId() >> userId
        0 * _
        thrown(ApplicationInstanceException)

        where:
        role << [ Role.PARTNER_ADMIN, Role.PARTNER_USER]
    }

}