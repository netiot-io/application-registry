package io.netiot.applicationregistry.UT.services

import feign.FeignException
import io.netiot.applicationregistry.exceptions.InternalErrorException
import io.netiot.applicationregistry.feign.UsersClient
import io.netiot.applicationregistry.services.UserService
import org.springframework.http.ResponseEntity
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import static io.netiot.applicationregistry.generators.UserGenerator.aUser
import static io.netiot.applicationregistry.generators.UserGenerator.aUserInternalModel


class UserServiceSpec extends Specification {

    def usersClient = Mock(UsersClient)
    def userService = new UserService(usersClient)

    @Shared
    def errorMessage = "error"

    def 'findUsersByIds'(){
        given:
        def userIds = [1L]
        def user = aUser()
        def userInternalModel = aUserInternalModel()
        def userResponseEntity = ResponseEntity.ok([userInternalModel])

        when:
        def result = userService.findUsersByIds(userIds)

        then:
        1 * usersClient.getUsers(userIds) >> userResponseEntity
        0 * _

        result == Optional.of([user])
    }

    def 'findUsersByIds return empty optional'(){
        given:
        def userIds = [1L]

        when:
        def result = userService.findUsersByIds(userIds)

        then:
        1 * usersClient.getUsers(userIds) >> { throw new FeignException(404, errorMessage) }
        0 * _

        result == Optional.empty()
    }

    @Unroll
    def 'findOrganizationById - throw InternalErrorException'(){
        given:
        def userIds = [1L]


        when:
        userService.findUsersByIds(userIds)

        then:
        1 * usersClient.getUsers(userIds) >> exception
        0 * _
        thrown(InternalErrorException)

        where:
        exception << [ { throw new FeignException(errorMessage) }, { throw new Exception() } ]
    }

}
