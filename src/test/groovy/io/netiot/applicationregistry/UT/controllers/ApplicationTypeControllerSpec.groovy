package io.netiot.applicationregistry.UT.controllers

import io.netiot.applicationregistry.controllers.ApplicationTypeController
import io.netiot.applicationregistry.services.ApplicationTypeService
import org.springframework.http.ResponseEntity
import spock.lang.Specification

import static io.netiot.applicationregistry.generators.ApplicationTypeGenerator.*

class ApplicationTypeControllerSpec extends Specification {

    def applicationTypeService = Mock(ApplicationTypeService)

    def applicationTypeController = new ApplicationTypeController(applicationTypeService)

    def "getApplicationsTypes"() {
        given:
        def applicationTypes = [ aApplicationTypeModel() ]
        def applicationTypeListModel = aApplicationTypeListModel()

        when:
        def result = applicationTypeController.getApplicationsTypes()
        then:
        1 * applicationTypeService.getApplicationsTypes() >> applicationTypes
        0 * _

        result == ResponseEntity.ok(applicationTypeListModel)
    }

}
