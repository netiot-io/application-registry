package io.netiot.applicationregistry.UT.controllers

import io.netiot.applicationregistry.controllers.ApplicationInstanceController
import io.netiot.applicationregistry.services.ApplicationInstanceService
import io.netiot.applicationregistry.validators.ValidatorUtil
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import spock.lang.Specification

import static io.netiot.applicationregistry.generators.ApplicationInstanceGenerator.*

class ApplicationInstanceControllerSpec extends Specification {

    def bindingResult = Mock(BindingResult)
    def validatorUtil = Mock(ValidatorUtil)
    def applicationInstanceService = Mock(ApplicationInstanceService)

    def applicationInstanceController = new ApplicationInstanceController(validatorUtil, applicationInstanceService)

    def "getApplicationInstancesForCurrentUser"() {
        given:
        def applicationInstanceModels = [ aApplicationInstanceModel() ]
        def applicationInstanceListModel = aApplicationInstanceListModel()

        when:
        def result = applicationInstanceController.getApplicationInstancesForCurrentUser()

        then:
        1 * applicationInstanceService.getApplicationInstancesCurrentUser() >> applicationInstanceModels
        0 * _

        result == ResponseEntity.ok(applicationInstanceListModel)
    }

    def "saveApplicationInstance"() {
        given:
        def applicationInstanceModel = aApplicationInstanceModel()

        when:
        def result = applicationInstanceController.saveApplicationInstance(applicationInstanceModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * applicationInstanceService.save(applicationInstanceModel) >> applicationInstanceModel
        0 * _

        result == ResponseEntity.ok([id : applicationInstanceModel.getId()])
    }

    def "updateApplicationInstance"() {
        given:
        def applicationInstanceModel = aApplicationInstanceModel()
        def applicationInstanceId = applicationInstanceModel.getId()

        when:
        def result = applicationInstanceController.updateApplicationInstance(applicationInstanceId, applicationInstanceModel, bindingResult)

        then:
        1 * validatorUtil.checkBindingResultErrors(bindingResult)
        1 * applicationInstanceService.update(applicationInstanceId, applicationInstanceModel)
        0 * _

        result == ResponseEntity.ok().build()
    }

    def "deleteApplicationInstance"(){
        given:
        def applicationInstanceId = 1L

        when:
        def result = applicationInstanceController.deleteApplicationInstance(applicationInstanceId)

        then:
        1 * applicationInstanceService.delete(applicationInstanceId)
        0 * _

        result == ResponseEntity.ok().build()
    }

}
