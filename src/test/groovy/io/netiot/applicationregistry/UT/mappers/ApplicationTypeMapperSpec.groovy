package io.netiot.applicationregistry.UT.mappers

import io.netiot.applicationregistry.mappers.ApplicationTypeMapper
import spock.lang.Specification

import static io.netiot.applicationregistry.generators.ApplicationTypeGenerator.aApplicationType
import static io.netiot.applicationregistry.generators.ApplicationTypeGenerator.aApplicationTypeListModel
import static io.netiot.applicationregistry.generators.ApplicationTypeGenerator.aApplicationTypeModel

class ApplicationTypeMapperSpec extends Specification {

    def 'toModel'() {
        given:
        def entity = aApplicationType()
        def model = aApplicationTypeModel()

        when:
        def result = ApplicationTypeMapper.toModel(entity)

        then:
        result == model
    }

    def 'toListModel'(){
        given:
        def models = [ aApplicationTypeModel() ]


        when:
        def result = ApplicationTypeMapper.toListModel(models)

        then:
        result == aApplicationTypeListModel()
    }

}
