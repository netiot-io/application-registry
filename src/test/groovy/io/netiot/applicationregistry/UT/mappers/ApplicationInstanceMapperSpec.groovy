package io.netiot.applicationregistry.UT.mappers

import io.netiot.applicationregistry.mappers.ApplicationInstanceMapper
import spock.lang.Specification

import static io.netiot.applicationregistry.generators.ApplicationInstanceGenerator.*

class ApplicationInstanceMapperSpec extends Specification {

    def 'toModel'() {
        given:
        def entity = aApplicationInstance()
        def model = aApplicationInstanceModel('organizations': null)

        when:
        def result = ApplicationInstanceMapper.toModel(entity)

        then:
        result == model
    }

    def 'toEntity'() {
        given:
        def entity = aApplicationInstance(applicationType: null)
        def model = aApplicationInstanceModel()

        when:
        def result = ApplicationInstanceMapper.toEntity(model)

        then:
        result == entity
    }

    def 'toListModel'(){
        given:
        def models = [ aApplicationInstanceModel() ]


        when:
        def result = ApplicationInstanceMapper.toListModel(models)

        then:
        result == aApplicationInstanceListModel()
    }

}
